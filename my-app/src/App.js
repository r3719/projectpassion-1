
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Nav from './Nav';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
      
          <Route path="calendar">
            <Route path="new" element={<Calendar />} />
          </Route>
          </Routes>
          </div>
          </BrowserRouter>
  )}
  ;

  export default App;
